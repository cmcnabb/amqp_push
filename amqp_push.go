package main

import (
	"log"
	"os"

	"github.com/streadway/amqp"
)

func main() {
	if len(os.Args) < 4 {
		log.Fatal("Usage:  amqp_push url exchange message")
	}
	conn, err := amqp.Dial(os.Args[1])
	failOnError(err, "Failed to connect to RabbitMQ")
	defer conn.Close()

	ch, err := conn.Channel()
	failOnError(err, "Failed to open a channel")
	defer ch.Close()

	err = ch.ExchangeDeclare(
		os.Args[2], // name
		"fanout",   // type
		true,       // durable
		false,      // auto-deleted
		false,      // internal
		false,      // no-wait
		nil,        // arguments
	)
	failOnError(err, "Failed to declare an exchange")

	body := os.Args[3]
	err = ch.Publish(
		os.Args[2], // exchange
		"",         // routing key
		false,      // mandatory
		false,      // immediate
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        []byte(body),
		})
	failOnError(err, "Failed to publish a message")
}
